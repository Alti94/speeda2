package works.noob.alti.speeda2;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import works.noob.alti.speeda2.units.TimeConverter;
import works.noob.alti.speeda2.units.Unit;
import works.noob.alti.speeda2.units.UnitFactory;

public class SpeedometerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "SPEEDA2";

    private static SpeedometerActivity instance;

    private SharedPreferences preferences;

    public static SpeedometerActivity getInstance() {
        return instance;
    }

    private Unit units;

    boolean isRotationLock;
    boolean isScreenLock;
    boolean isPaused;

    private Intent gpsServiceIntent;

    private NotificationManager notificationManager;
    private Notification gpsNotification;
    private static final int NOTIFICATION_ID = 66;

    BroadcastReceiver gpsReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TextView speedText = (TextView) findViewById(R.id.speed);
            TextView trackTimeText = (TextView) findViewById(R.id.track_time) ;
            TextView distanceText = (TextView) findViewById(R.id.distance);
            TextView avgSpeedText = (TextView) findViewById(R.id.avg_speed);
            TextView maxSpeedText = (TextView) findViewById(R.id.max_speed);
            TextView altitudeText = (TextView) findViewById(R.id.altitude);

            speedText.setText(units.convertSpeed(intent.getDoubleExtra("speed", 0)));
            distanceText.setText(units.convertDistance(intent.getDoubleExtra("totalDistance", 0)));
            avgSpeedText.setText(units.convertSpeed(intent.getDoubleExtra("avgSpeed", 0)));
            maxSpeedText.setText(units.convertSpeed(intent.getDoubleExtra("maxSpeed", 0)));
            altitudeText.setText(units.convertAltitude(intent.getDoubleExtra("altitude", 0)));
            trackTimeText.setText(TimeConverter.convert(intent.getLongExtra("totalTime", 0)));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        units = UnitFactory.createUnit(preferences.getString(SettingsActivity.UNITS, "kilometer"));

        initUi();

        //requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        //if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        //}

        isRotationLock = false;
        isScreenLock = false;
        isPaused = false;

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        notification.setContentTitle(getString(R.string.app_name));
        notification.setContentText(getString(R.string.notification_text));
        notification.setOngoing(true);
        notification.setSmallIcon(R.drawable.ic_notification);

        gpsNotification = notification.build();
    }

    private void initUi() {
        setContentView(R.layout.activity_speedometer);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        updateColors();
    }

    private void updateColors() {
        updateLabelFontColor();
        updateValueFontColor();
        updateBackgroundColor();
    }

    public void updateBackgroundColor() {
        RelativeLayout root = (RelativeLayout) findViewById(R.id.content_main);
        int backgroundColor = preferences.getInt(SettingsActivity.BACKGOUND_COLOR, -1);
        root.setBackgroundColor(backgroundColor);
    }

    public void updateLabelFontColor() {
        TextView speedLabel = (TextView) findViewById(R.id.speed_label);
        TextView trackTimeLabel = (TextView) findViewById(R.id.track_time_label);
        TextView actualTimeLabel = (TextView) findViewById(R.id.actual_time_label);
        TextView distanceLabel = (TextView) findViewById(R.id.distance_label);
        TextView avgSpeedLabel = (TextView) findViewById(R.id.avg_speed_label);
        TextView maxSpeedLabel = (TextView) findViewById(R.id.max_speed_label);
        TextView altitudeLabel = (TextView) findViewById(R.id.altitude_label);

        int fontColor = preferences.getInt(SettingsActivity.LABEL_FONT_COLOR, -1);

        speedLabel.setTextColor(fontColor);
        trackTimeLabel.setTextColor(fontColor);
        actualTimeLabel.setTextColor(fontColor);
        distanceLabel.setTextColor(fontColor);
        avgSpeedLabel.setTextColor(fontColor);
        maxSpeedLabel.setTextColor(fontColor);
        altitudeLabel.setTextColor(fontColor);
    }

    public void updateValueFontColor() {
        TextClock actualTimeText = (TextClock) findViewById(R.id.actual_time);
        TextView speedText = (TextView) findViewById(R.id.speed);
        TextView trackTimeText = (TextView) findViewById(R.id.track_time) ;
        TextView distanceText = (TextView) findViewById(R.id.distance);
        TextView avgSpeedText = (TextView) findViewById(R.id.avg_speed);
        TextView maxSpeedText = (TextView) findViewById(R.id.max_speed);
        TextView altitudeText = (TextView) findViewById(R.id.altitude);

        int fontColor = preferences.getInt(SettingsActivity.VALUE_FONT_COLOR, -1);

        actualTimeText.setTextColor(fontColor);
        speedText.setTextColor(fontColor);
        trackTimeText.setTextColor(fontColor);
        distanceText.setTextColor(fontColor);
        avgSpeedText.setTextColor(fontColor);
        maxSpeedText.setTextColor(fontColor);
        altitudeText.setTextColor(fontColor);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(gpsReciver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(gpsReciver, new IntentFilter(GpsService.GPS_DATA));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length == 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.permission_required), Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    gpsServiceIntent = new Intent(this, GpsService.class);
                    startService(gpsServiceIntent);
                    notificationManager.notify(NOTIFICATION_ID, gpsNotification);
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        stopService(gpsServiceIntent);
        notificationManager.cancel(NOTIFICATION_ID);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
            //super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_pause_resume:
                pauseResumeRecording(item);
                break;

            case R.id.nav_new_track:
                newTrack();
                break;

            case R.id.nav_display_lock:
                displayLock(item);
                break;

            case R.id.nav_rotation_lock:
                lockRotation(item);
                break;

            case R.id.nav_app_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;

            case R.id.nav_app_exit:
                super.onBackPressed();
                break;

            case R.id.nav_map_mode:
                startActivity(new Intent(this, MapActivity.class));
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final CharSequence speed = ((TextView) findViewById(R.id.speed)).getText();
        final CharSequence trackTime = ((TextView) findViewById(R.id.track_time)).getText();
        final CharSequence distance = ((TextView) findViewById(R.id.distance)).getText();
        final CharSequence avgSpeed = ((TextView) findViewById(R.id.avg_speed)).getText();
        final CharSequence maxSpeed = ((TextView) findViewById(R.id.max_speed)).getText();
        final CharSequence altitude = ((TextView) findViewById(R.id.altitude)).getText();

        ((DrawerLayout) findViewById(R.id.drawer_layout)).removeAllViews();
        super.onConfigurationChanged(newConfig);
        initUi();

        ((TextView) findViewById(R.id.speed)).setText(speed);
        ((TextView) findViewById(R.id.track_time)).setText(trackTime);
        ((TextView) findViewById(R.id.distance)).setText(distance);
        ((TextView) findViewById(R.id.avg_speed)).setText(avgSpeed);
        ((TextView) findViewById(R.id.max_speed)).setText(maxSpeed);
        ((TextView) findViewById(R.id.altitude)).setText(altitude);
    }

    private void pauseResumeRecording(MenuItem item) {
        if (isPaused = !isPaused) {
            GpsService.getInstance().timerPause();
            item.setTitle("Resume");
            item.setIcon(R.drawable.ic_menu_resume);
        }
        else {
            GpsService.getInstance().timerResume();
            item.setTitle("Pause");
            item.setIcon(R.drawable.ic_menu_pause);
        }
    }

    private void displayLock(MenuItem item) {
        if (isScreenLock = !isScreenLock) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            item.setTitle(getString(R.string.unlock_screen));
            item.setIcon(R.drawable.ic_menu_screen_unlock);
        }
        else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            item.setTitle(getString(R.string.lock_screen));
            item.setIcon(R.drawable.ic_menu_screen_lock);
        }
    }

    private void lockRotation(MenuItem item) {
        if (!isRotationLock) {
            setRequestedOrientation(getScreenOrientation());
            item.setIcon(R.drawable.ic_menu_unlock_rotation);
            item.setTitle(getString(R.string.unlock_rotation));
            isRotationLock = true;
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            item.setIcon(R.drawable.ic_menu_lock_rotation);
            item.setTitle(getString(R.string.lock_rotation));
            isRotationLock = false;
        }
    }

    public int getScreenOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    Log.e(TAG, "Unknown screen orientation. Defaulting to " + "portrait.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        else {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                    Log.e(TAG, "Unknown screen orientation. Defaulting to " + "landscape.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }

    private void newTrack() {
        stopService(gpsServiceIntent);
        gpsServiceIntent = new Intent(this, GpsService.class);
        gpsReciver.onReceive(this, new Intent());
        startService(gpsServiceIntent);

        MenuItem item = ((NavigationView) findViewById(R.id.nav_view)).getMenu()
                .findItem(R.id.nav_pause_resume);

        item.setTitle("Pause");
        item.setIcon(R.drawable.ic_menu_pause);
        isPaused = false;
    }

    public void setUnits(Unit units) {
        this.units = units;
    }
}
