package works.noob.alti.speeda2.export_track;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import works.noob.alti.speeda2.App;
import works.noob.alti.speeda2.R;
import works.noob.alti.speeda2.database.Point;
import works.noob.alti.speeda2.database.Track;

/**
 * Created by kamil on 29.12.16.
 */

public class ExportGpx implements TrackExport {
    private static final String TAG = "GPX_EXPORT";
    private static final DateFormat pointDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private static final DateFormat nameDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    private OutputStream stream;
    private XmlSerializer xml;
    private Integer segmentNumber;

    private final String fileName;

    public ExportGpx(Track track, File exportDir) {
        fileName = nameDateFormat.format(track.getStartDate()) + ".gpx";
        File file = new File(exportDir, fileName);

        try {
            file.createNewFile();
            stream = new FileOutputStream(file);

            xml = Xml.newSerializer();

            xml.setOutput(stream, "UTF-8");
            xml.startDocument("UTF-8", true);
            xml.startTag("", "gpx");
            xml.attribute("xmlns", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xml.attribute("", "xmlns", "http://www.topografix.com/GPX/1/1");
            xml.attribute("xsi", "schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd");
            xml.attribute("", "version", "1.1");
            xml.attribute("", "creator", App.getInstance().getApplicationContext().getString(R.string.app_name));
        } catch (IOException e) {
            Log.e(TAG, "Can't create file");
            //e.printStackTrace();
        }

    }

    @Override
    public void addPoint(Point point) {
        if (stream != null) {
            if (segmentNumber == null) {
                segmentNumber = point.getSegmentNumber();
                startSegment();
            }

            if (!segmentNumber.equals(point.getSegmentNumber())) {
                segmentNumber = point.getSegmentNumber();
                endSegment();
                startSegment();
            }

            try {
                xml.startTag("", "trkpt");
                xml.attribute("", "lat", point.getLatitude().toString());
                xml.attribute("", "lon", point.getLongitude().toString());
                xml.startTag("", "ele");
                xml.text(point.getAltitude().toString());
                xml.endTag("", "ele");
                xml.startTag("", "time");
                xml.text(pointDateFormat.format(point.getDate()));
                xml.endTag("", "time");
                xml.endTag("", "trkpt");
            } catch (IOException e) {
                Log.e(TAG, "Point write IO error");
                //e.printStackTrace();
            }
        }
    }

    @Override
    public String endTrack() {
        if (stream != null) {
            endSegment();
            try {
                xml.endTag("", "gpx");
                xml.endDocument();
                stream.flush();
                stream.close();
                stream = null;
            } catch (IOException e) {
                Log.e(TAG, "End track IO error");
                //e.printStackTrace();
            }
        }
        return fileName;
    }

    private void startSegment() {
        try {
            xml.startTag("", "trk");
            xml.startTag("", "name");
            xml.text("segment " + segmentNumber.toString());
            xml.endTag("", "name");
            xml.startTag("", "trkseg");
        } catch (IOException e) {
            Log.e(TAG, "Start segment IO error");
            //e.printStackTrace();
        }
    }

    private void endSegment() {
        try {
            xml.endTag("", "trkseg");
            xml.endTag("", "trk");
        } catch (IOException e) {
            Log.e(TAG, "End segment IO error");
            //e.printStackTrace();
        }
    }
}
