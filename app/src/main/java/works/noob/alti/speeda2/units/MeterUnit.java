package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 25.12.16.
 */

public class MeterUnit extends Unit {
    private static final String DISTANCE_UNIT = "m";
    private static final String SPEED_UNIT = "m/s";
    private static final String ALTITUDE_UNIT = "m";

    @Override
    public String getDistanceUnit() {
        return DISTANCE_UNIT;
    }

    @Override
    public String getSpeedUnit() {
        return SPEED_UNIT;
    }

    @Override
    public String getAltitudeUnit() {
        return ALTITUDE_UNIT;
    }

    @Override
    public String getDistanceValue(double distanceInMeter) {
        return String.format("%.1f", distanceInMeter);
    }

    @Override
    public String getSpeedValue(double speedInMeterPerSecond) {
        return String.format("%.1f", speedInMeterPerSecond);
    }

    @Override
    public String getAltitudeValue(double altitudeInMeter) {
        return String.format("%.1f", altitudeInMeter);
    }
}
