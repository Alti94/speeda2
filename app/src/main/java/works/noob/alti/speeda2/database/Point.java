package works.noob.alti.speeda2.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.mapsforge.core.model.LatLong;

import java.util.Date;

/**
 * Created by kamil on 24.12.16.
 */

@Entity
public class Point {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;

    @NotNull
    private Double altitude;

    @NotNull
    private Double speed;

    @NotNull
    private Double distance;

    @NotNull
    private Long time;

    @NotNull
    private Date date;

    @NotNull Integer segmentNumber;

    @NotNull
    private Long trackId;

    @Generated(hash = 606163459)
    public Point(Long id, @NotNull Double latitude, @NotNull Double longitude,
            @NotNull Double altitude, @NotNull Double speed,
            @NotNull Double distance, @NotNull Long time, @NotNull Date date,
            @NotNull Integer segmentNumber, @NotNull Long trackId) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.distance = distance;
        this.time = time;
        this.date = date;
        this.segmentNumber = segmentNumber;
        this.trackId = trackId;
    }

    @Generated(hash = 1977038299)
    public Point() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return this.altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return this.speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getDistance() {
        return this.distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getTrackId() {
        return this.trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    public LatLong getLatLong() {
        if (latitude != null && longitude != null) {
            LatLong latLong = new LatLong(latitude, longitude);
            return latLong;
        }
        throw new NullPointerException("Null coords");
    }

    public Long getTime() {
        return this.time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getSegmentNumber() {
        return this.segmentNumber;
    }

    public void setSegmentNumber(Integer segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
