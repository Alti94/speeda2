package works.noob.alti.speeda2;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import works.noob.alti.speeda2.database.DaoSession;
import works.noob.alti.speeda2.database.PointDao;
import works.noob.alti.speeda2.database.Track;
import works.noob.alti.speeda2.database.TrackDao;
import works.noob.alti.speeda2.units.TimeConverter;
import works.noob.alti.speeda2.units.Unit;
import works.noob.alti.speeda2.units.UnitFactory;

/**
 * Created by kamil on 27.12.16.
 */

public class TrackSelectActivity extends ListActivity {
    private ArrayAdapter<Track> trackAdapter;

    private SharedPreferences preferences;
    private Unit units;
    private List<Track> elements;
    private DaoSession session;

    public static final String TRACK_ID = "track_id";

    private static final int DELETE_MENU_ITEM_ID = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        session = ((App) getApplication()).getDaoSession();

        TrackDao trackDao = session.getTrackDao();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        units = UnitFactory.createUnit(preferences.getString(SettingsActivity.UNITS, null));

        Long actualTrackId = GpsService.getInstance().getActualTrackId();

        elements = trackDao.queryBuilder().where(TrackDao.Properties.Id.notEq(actualTrackId)).orderDesc(TrackDao.Properties.StartDate).list();
        trackAdapter = new ArrayAdapter<Track>(this, 0, elements) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder viewHolder;

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.track_element, parent, false);

                    viewHolder = new ViewHolder();
                    viewHolder.dateText = (TextView) convertView.findViewById(R.id.date);
                    viewHolder.timeText = (TextView) convertView.findViewById(R.id.time);
                    viewHolder.distanceText = (TextView) convertView.findViewById(R.id.distance);
                    viewHolder.avgSpeedText = (TextView) convertView.findViewById(R.id.avg_speed);
                    viewHolder.maxSpeedText = (TextView) convertView.findViewById(R.id.max_speed);

                    convertView.setTag(viewHolder);
                }
                else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }

                Track item = getItem(position);
                if (item != null) {
                    viewHolder.dateText.setText(item.getStartDate().toString());
                    viewHolder.timeText.setText(TimeConverter.convert(item.getTrackTime()));
                    viewHolder.distanceText.setText(units.convertDistance(item.getDistance()));
                    viewHolder.avgSpeedText.setText(units.convertSpeed(item.getAvgSpeed()));
                    viewHolder.maxSpeedText.setText(units.convertSpeed(item.getMaxSpeed()));
                }

                return convertView;
            }


        };

        setListAdapter(trackAdapter);

        registerForContextMenu(getListView());

//        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                final Track track = trackAdapter.getItem(position);
//                elements.remove(track);
//                trackAdapter.notifyDataSetChanged();
//                new Thread() {
//                    @Override
//                    public void run() {
//                        PointDao pointDao = session.getPointDao();
//                        pointDao.deleteInTx(track.getPointList());
//                        track.delete();
//                    }
//                }.start();
//                return true;
//            }
//        });
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Track item = (Track) l.getItemAtPosition(position);
        Intent data = new Intent();
        data.putExtra(TRACK_ID, item.getId());
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v == getListView()) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(getString(R.string.action));

            menu.add(Menu.NONE, DELETE_MENU_ITEM_ID, 0, getString(R.string.delete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuOption = item.getItemId();
        if (menuOption == DELETE_MENU_ITEM_ID) {
            final Track track = trackAdapter.getItem(info.position);
                elements.remove(track);
                trackAdapter.notifyDataSetChanged();
                new Thread() {
                    @Override
                    public void run() {
                        PointDao pointDao = session.getPointDao();
                        pointDao.deleteInTx(track.getPointList());
                        track.delete();
                    }
                }.start();
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private static class ViewHolder {
        private TextView dateText;
        private TextView timeText;
        private TextView distanceText;
        private TextView avgSpeedText;
        private TextView maxSpeedText;
    }

    @Override
    protected void onDestroy() {
        unregisterForContextMenu(getListView());
        super.onDestroy();
    }
}
