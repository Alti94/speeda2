package works.noob.alti.speeda2;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.greenrobot.greendao.database.Database;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import works.noob.alti.speeda2.database.DaoMaster;
import works.noob.alti.speeda2.database.DaoSession;

/**
 * Created by kamil on 24.12.16.
 */

public class App extends Application {
    private DaoSession daoSession;

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        PreferenceManager.setDefaultValues(this, R.xml.pref_main, false);
        PreferenceManager.setDefaultValues(this, R.xml.pref_speedometer, true);
        PreferenceManager.setDefaultValues(this, R.xml.pref_map, true);

        AndroidGraphicFactory.createInstance(this);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "database");
        Database database = helper.getWritableDb();
        daoSession = new DaoMaster(database).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public static App getInstance() {
        return instance;
    }
}
