package works.noob.alti.speeda2;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.File;
import java.io.FilenameFilter;

import works.noob.alti.speeda2.R;

/**
 * Created by kamil on 09.01.17.
 */

public class FileSelectActivity extends ListActivity {
    public static final String FILENAME = "filename";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        File appDir = new File(Environment.getExternalStorageDirectory(), "Speeda2");
        String[] files = appDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".gpx");
            }
        });

        ListAdapter listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, files);
        setListAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String filename = (String) l.getItemAtPosition(position);
        Intent data = new Intent();
        data.putExtra(FILENAME, filename);
        setResult(RESULT_OK, data);
        finish();
    }
}
