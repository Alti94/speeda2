package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 26.12.16.
 */

public class UnitFactory {
    public static final String UNIT_METERS = "meter";
    public static final String UNIT_KILOMETERS = "kilometer";
    public static final String UNIT_MILES = "mile";

    private UnitFactory() {}

    public static final Unit createUnit(String unitName) {
        switch (unitName) {
            case UNIT_METERS:
                return new MeterUnit();
            case UNIT_KILOMETERS:
                return new KiloMeterUnit();
            case UNIT_MILES:
                return new MilesUnit();
        }
        throw new IllegalArgumentException("Unit '" + unitName + "' not exists!");
    }
}
