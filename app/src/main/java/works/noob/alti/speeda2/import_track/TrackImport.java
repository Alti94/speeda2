package works.noob.alti.speeda2.import_track;

import java.io.File;

import works.noob.alti.speeda2.database.Track;

/**
 * Created by kamil on 08.01.17.
 */

public abstract class TrackImport {
    protected static final String TAG = "IMPORT_TRACK";

    public abstract Track importTrack(File file);
}
