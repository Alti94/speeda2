package works.noob.alti.speeda2.units;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import works.noob.alti.speeda2.TrackStatisticsActivity;

/**
 * Created by kamil on 31.12.16.
 */

public class ChartUnitsFactory {
    private ChartUnitsFactory() {}

    public static IAxisValueFormatter createChartUnit(int unitType, Unit unit) {
        switch (unitType) {
            case TrackStatisticsActivity.SPEED_AXIS:
                return new SpeedFormatter(unit);
            case TrackStatisticsActivity.ALTITUDE_AXIS:
                return new AltitudeFormatter(unit);
            case TrackStatisticsActivity.DISTANCE_AXIS:
                return new DistanceFormatter(unit);
            case TrackStatisticsActivity.TIME_AXIS:
                return new TimeFormatter();
        }
        throw new IllegalArgumentException("Type do not exists");
    }

    private static class SpeedFormatter implements IAxisValueFormatter {
        private Unit units;

        public SpeedFormatter(Unit units) {
            this.units = units;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return units.getSpeedValue((double) value);
        }
    }

    private static class DistanceFormatter implements IAxisValueFormatter {
        private Unit units;

        public DistanceFormatter(Unit units) {
            this.units = units;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return units.getDistanceValue((double) value);
        }
    }

    private static class TimeFormatter implements IAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return TimeConverter.convert((long) value);
        }
    }

    private static class AltitudeFormatter implements IAxisValueFormatter {
        private Unit units;

        public AltitudeFormatter(Unit units) {
            this.units = units;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return units.getAltitudeValue((double) value);
        }
    }
}

