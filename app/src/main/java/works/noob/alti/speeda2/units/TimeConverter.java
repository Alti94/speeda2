package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 25.12.16.
 */

public class TimeConverter {
    public static String convert(long time) {
        time /= 1000;
        long seconds = time % 60; // sekundy
        time /= 60;
        long minutes = time % 60; // minuty
        long hours = time / 60; // godziny

        return String.format("%d:%02d:%02d", hours, minutes, seconds);
    }
}
