package works.noob.alti.speeda2.import_track;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import works.noob.alti.speeda2.App;
import works.noob.alti.speeda2.database.DaoSession;
import works.noob.alti.speeda2.database.Point;
import works.noob.alti.speeda2.database.PointDao;
import works.noob.alti.speeda2.database.Track;
import works.noob.alti.speeda2.database.TrackDao;

/**
 * Created by kamil on 08.01.17.
 */

public class ImportGpx extends TrackImport {
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private XmlPullParser parser;
    private App application;

    private int segmentNumber;
    private long trackTime;
    private double distance;

    private TrackDao trackDao;
    private PointDao pointDao;

    private Track track;
    private Point startPoint;
    private Point endPoint;

    public ImportGpx() {
        segmentNumber = 0;
        trackTime = 0;
        distance = 0;

        startPoint = null;
        endPoint = null;

        application = App.getInstance();
        DaoSession daoSession = application.getDaoSession();
        trackDao = daoSession.getTrackDao();
        pointDao = daoSession.getPointDao();


    }

    @Override
    public Track importTrack(File file) {
        Date initDate = new Date();
        Double initDouble = 0d;
        Long initLong = 0L;

        track = new Track(null, initDate, initDate, initDouble, initDouble, initDouble, initLong);
        trackDao.insert(track);

        try {
            InputStream inputStream = new FileInputStream(file);
            readTrack(inputStream);

            if (startPoint != null && startPoint != endPoint) {
                track.setStartDate(startPoint.getDate());
                track.setEndDate(endPoint.getDate());

                double avgSpeed = distance / (trackTime / 1000f);
                track.setAvgSpeed(avgSpeed);

                track.update();
            }
            else {
                pointDao.deleteInTx(track.getPointList());
                track.delete();
                track = null;
            }
            track.update();
            inputStream.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found");
            track.delete();
            track = null;
        } catch (NumberFormatException|XmlPullParserException|ParseException e) {
            Log.e(TAG, "Parsing error");
            pointDao.deleteInTx(track.getPointList());
            track.delete();
            track = null;
        } catch (IOException e) {
            Log.e(TAG, "IO error");
            pointDao.deleteInTx(track.getPointList());
            track.delete();
            track = null;
        }

        return track;
    }



    private void readTrack(InputStream stream) throws IOException, XmlPullParserException, ParseException, NumberFormatException {
        parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(stream, null);

        parser.nextTag();

        parser.require(XmlPullParser.START_TAG, null, "gpx");

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;

            String tag = parser.getName();
            if (tag.equals("trk")) {
                readSegment();
            }
            else {
                skipTags();
            }
        }
    }

    private void readSegment() throws IOException, XmlPullParserException, ParseException, NumberFormatException {
        parser.require(XmlPullParser.START_TAG, null, "trk");

        segmentNumber++;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;

            String tag = parser.getName();

            if (tag.equals("trkseg")) {
                readPoints();
            }
            else {
                skipTags();
            }
        }
    }

    private void readPoints() throws IOException, XmlPullParserException, ParseException, NumberFormatException {
        parser.require(XmlPullParser.START_TAG, null, "trkseg");

        Point lastPoint = null;

        List<Point> points = new ArrayList<>();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;

            String tag = parser.getName();

            if (tag.equals("trkpt")) {
                Point point = readPoint();
                point.setSegmentNumber(segmentNumber);

                double pointToPointDistance = distenceBetweenPoints(point, lastPoint);
                distance += pointToPointDistance;
                point.setDistance(distance);
                track.setDistance(distance);

                double speed = getSpeedBetweenPoints(lastPoint, point, pointToPointDistance);
                point.setSpeed(speed);
                if (speed > track.getMaxSpeed()) {
                    track.setMaxSpeed(speed);
                }

                trackTime += getTimeBetweenPoints(lastPoint, point);
                point.setTime(trackTime);
                track.setTrackTime(trackTime);

                point.setTrackId(track.getId());
                points.add(point);

                if (startPoint == null) startPoint = point;
                endPoint = point;

                lastPoint = point;
            }
            else {
                skipTags();
            }
        }

        pointDao.insertInTx(points);
    }

    private Point readPoint() throws IOException, XmlPullParserException, ParseException, NumberFormatException {
        parser.require(XmlPullParser.START_TAG, null, "trkpt");

        Point point = new Point();

        String textLatitude = parser.getAttributeValue(null, "lat");
        String textLongitude = parser.getAttributeValue(null, "lon");
        point.setLatitude(Double.valueOf(textLatitude));
        point.setLongitude(Double.valueOf(textLongitude));

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;

            String tag = parser.getName();
            if (tag.equals("ele")) {
                parser.require(XmlPullParser.START_TAG, null, "ele");
                String textAltitude = readContent();
                point.setAltitude(Double.valueOf(textAltitude));
                parser.require(XmlPullParser.END_TAG, null, "ele");
            } else if (tag.equals("time")) {
                parser.require(XmlPullParser.START_TAG, null, "time");
                String textData = readContent();
                parser.require(XmlPullParser.END_TAG, null, "time");
                Date pointDate = dateFormat.parse(textData);
                point.setDate(pointDate);
            } else {
                skipTags();
            }
        }

        return point;
    }

    private String readContent() throws IOException, XmlPullParserException {
        String text = "";
        if (parser.next() == XmlPullParser.TEXT) {
            text = parser.getText();
            parser.nextTag();
        }
        return text;
    }

    private void skipTags() throws IOException, XmlPullParserException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private static double distenceBetweenPoints(Point point1, Point point2) {
        if (point1 == null || point2 == null)
            return 0;

        double R = 6371000f; // Radius of the earth in m
        double dLat = (point1.getLatitude() - point2.getLatitude()) * Math.PI / 180f;
        double dLon = (point1.getLongitude() - point2.getLongitude()) * Math.PI / 180f;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(point1.getLatitude() * Math.PI / 180f) * Math.cos(point2.getLatitude() * Math.PI / 180f) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2f * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
    }

    private static long getTimeBetweenPoints(Point lastPoint, Point point) {
        if (lastPoint == null || point == null)
            return 0;
        return point.getDate().getTime() - lastPoint.getDate().getTime();
    }

    private static double getSpeedBetweenPoints(Point lastPoint, Point point, double distance) {
        if (lastPoint == null || point == null)
            return 0;
        double timeBetween = getTimeBetweenPoints(lastPoint, point);
        timeBetween /= 1000f;
        if (timeBetween == 0)
            timeBetween = 1;

        return distance / timeBetween;
    }
}
