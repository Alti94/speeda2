package works.noob.alti.speeda2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultFillFormatter;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import works.noob.alti.speeda2.database.Point;
import works.noob.alti.speeda2.database.PointDao;
import works.noob.alti.speeda2.units.ChartUnitsFactory;
import works.noob.alti.speeda2.units.Unit;
import works.noob.alti.speeda2.units.UnitFactory;

/**
 * Created by kamil on 30.12.16.
 */

public class ChartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chart);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Unit units = UnitFactory.createUnit(preferences.getString(SettingsActivity.UNITS, null));

        Intent activityIntent = getIntent();
        int axisX = activityIntent.getIntExtra(TrackStatisticsActivity.AXIS_X, 0);
        int axisY = activityIntent.getIntExtra(TrackStatisticsActivity.AXIS_Y, 0);
        long trackId = activityIntent.getLongExtra(TrackStatisticsActivity.TRACK_ID, -1);
        int selectedSegment = activityIntent.getIntExtra(TrackStatisticsActivity.SELECTED_SEGMENT, 0);

        setAxisLabels(axisX, axisY, units);
        List<Point> segment = getSegment(trackId, selectedSegment);

        composeChart(segment, units, axisX, axisY);
    }

    private void setAxisLabels(int x, int y, Unit units) {
        String axisX;
        String axisY;

        switch (x) {
            case TrackStatisticsActivity.DISTANCE_AXIS:
                axisX = getString(R.string.chart_distance) +
                        " [" + units.getDistanceUnit() + "]";
                break;
            case TrackStatisticsActivity.TIME_AXIS:
                axisX = getString(R.string.chart_time) +
                        " [" + "s" + "]";
                break;
            default:
                axisX = "??? [?]";
                break;
        }

        switch (y) {
            case TrackStatisticsActivity.ALTITUDE_AXIS:
                axisY = getString(R.string.chart_altitude) +
                        " [" + units.getAltitudeUnit() + "]";
                break;
            case TrackStatisticsActivity.SPEED_AXIS:
                axisY = getString(R.string.chart_speed) +
                        " [" + units.getSpeedUnit() + "]";
                break;
            default:
                axisY = "??? [?]";
                break;
        }

        ((TextView) findViewById(R.id.axis_x)).setText(axisX);
        ((TextView) findViewById(R.id.axis_y)).setText(axisY);
    }

    private List<Point> getSegment(long trackId, int segment) {
        PointDao pointDao = ((App) getApplication()).getDaoSession().getPointDao();
        QueryBuilder queryBuilder = pointDao.queryBuilder();

        if (segment == 0)
            queryBuilder.where(PointDao.Properties.TrackId.eq(trackId));
        else
            queryBuilder.where(
                    queryBuilder.and(
                            PointDao.Properties.TrackId.eq(trackId),
                            PointDao.Properties.SegmentNumber.eq(segment)
                    )
            );
        queryBuilder.orderAsc(PointDao.Properties.Id);
        return queryBuilder.list();
    }

    private void composeChart(List<Point> segment, Unit units, int xType, int yType) {
        LineChart chart = (LineChart) findViewById(R.id.chart);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setPinchZoom(false);
        chart.getDescription().setText("");
        chart.getLegend().setEnabled(false);

        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        YAxis yAxis = chart.getAxisLeft();

        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(0xFFFFFFFF);

        yAxis.setDrawGridLines(false);
        yAxis.setAxisMinimum(0);
        yAxis.setTextColor(0xFFFFFFFF);

        xAxis.setValueFormatter(ChartUnitsFactory.createChartUnit(xType, units));
        yAxis.setValueFormatter(ChartUnitsFactory.createChartUnit(yType, units));

        List<Entry> entries = dataInflate(new ArrayList<Entry>(), segment, xType, yType);
        LineDataSet dataSet = new LineDataSet(entries, "data");
        dataSet.setColor(Color.RED);
        dataSet.setLineWidth(0.5f);
        dataSet.setDrawValues(false);
        dataSet.setDrawCircles(false);
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setFillAlpha(128);
        dataSet.setDrawFilled(true);
        dataSet.setFillColor(Color.RED);
        dataSet.setValueTextColor(0xFFFFFFFF);

        dataSet.setFillFormatter(new DefaultFillFormatter());


        LineData data = new LineData(dataSet);
        chart.setData(data);
    }

    private List<Entry> dataInflate(List<Entry> entries, List<Point> segment, int xType, int yType) {
        for (Point point : segment)
            entries.add(
                    new Entry(
                            selectData(point, xType),
                            selectData(point, yType)
                    )
            );
        return entries;
    }

    private static float selectData(Point point, int type) {
        switch (type) {
            case TrackStatisticsActivity.SPEED_AXIS:
                return point.getSpeed().floatValue();
            case TrackStatisticsActivity.ALTITUDE_AXIS:
                return point.getAltitude().floatValue();
            case TrackStatisticsActivity.DISTANCE_AXIS:
                return point.getDistance().floatValue();
            case TrackStatisticsActivity.TIME_AXIS:
                return point.getTime().floatValue();
            default:
                return 0;
        }
    }
}
