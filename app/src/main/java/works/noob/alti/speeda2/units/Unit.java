package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 25.12.16.
 */

public abstract class Unit {
    public String convertDistance(double distanceInMeter) {
        return getDistanceValue(distanceInMeter) + " " + getDistanceUnit();
    }


    public String convertSpeed(double speedInMeterPerSecond) {
        return getSpeedValue(speedInMeterPerSecond) + " " + getSpeedUnit();
    }

    public String convertAltitude(double altitudeInMeter) {
        return getAltitudeValue(altitudeInMeter) + " " + getAltitudeUnit();
    }

    abstract public String getDistanceUnit();
    abstract public String getSpeedUnit();
    abstract public String getAltitudeUnit();
    abstract public String getDistanceValue(double distanceInMeter);
    abstract public String getSpeedValue(double speedInMeterPerSecond);
    abstract public String getAltitudeValue(double altitudeInMeter);
}
