package works.noob.alti.speeda2;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.Date;
import java.util.List;

import works.noob.alti.speeda2.database.DaoSession;
import works.noob.alti.speeda2.database.Point;
import works.noob.alti.speeda2.database.PointDao;
import works.noob.alti.speeda2.database.Track;
import works.noob.alti.speeda2.database.TrackDao;

/**
 * Created by kamil on 21.12.16.
 */

public class GpsService extends Service implements LocationListener, Runnable {
    public static final String GPS_DATA = "works.noob.alti.speeda2.GPS_DATA";
    private static final String TAG = "Serwis GPS";

    private LocationManager locationManager;
    private Location lastLocation;

    private boolean timerStatus;

    private double totalDistance;
    private double speed;
    private double maxSpeed;
    private double avgSpeed;
    private double altitude;
    private Long lastTime;
    private Long totalTime;

    private int segmentNumber;

    private Object lock;
    private Handler handler;
    private static GpsService instance;

    public static GpsService getInstance() {
        return instance;
    }

    private TrackDao trackDao;
    private PointDao pointDao;

    private Track track;

    SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        DaoSession session = ((App) getApplication()).getDaoSession();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        trackDao = session.getTrackDao();
        pointDao = session.getPointDao();

        instance = this;
        lock = new Object();
        handler = new Handler();

        totalDistance = 0;
        speed = 0;
        maxSpeed = 0;
        avgSpeed = 0;
        altitude = 0;
        totalTime = 0l;
        segmentNumber = 0;

        Date date = new Date();
        track = new Track(null, date, date, avgSpeed, maxSpeed, totalDistance, totalTime);
        trackDao.insert(track);

        timerStatus = false;
        if (preferences.getBoolean("autoStart", true))
            timerResume();

        handler.post(this);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "blad uprawnien gps (uruchamianie)");
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        Log.d(TAG, "nasluch gps poprawnie uruchomiony");
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "blad uprawnien gps (wylaczanie)");
            return;
        }
        locationManager.removeUpdates(this);
        Log.d(TAG, "nasluch gps poprawnie wylaczony");


        long count = pointDao.queryBuilder().where(PointDao.Properties.TrackId.eq(track.getId())).count();
        if (count < 1)
            track.delete();

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        synchronized (lock) {
            speed = location.getSpeed(); // prędkość w m/s
            altitude = location.getAltitude(); // wysokosc w m

            if (timerStatus) {
                if (lastLocation != null)
                    totalDistance += location.distanceTo(lastLocation); // dystans w m

                if (speed > maxSpeed)
                    maxSpeed = speed;


                Date date = new Date();
                track.setDistance(totalDistance);
                track.setMaxSpeed(maxSpeed);
                track.setAvgSpeed(avgSpeed);
                track.setTrackTime(totalTime);
                track.setEndDate(date);
                track.update();

                Point point = new Point(
                        null,
                        location.getLatitude(),
                        location.getLongitude(),
                        altitude,
                        speed,
                        totalDistance,
                        totalTime,
                        date,
                        segmentNumber,
                        track.getId()
                );
                pointDao.insert(point);
            }

            lastLocation = location;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public void timerResume() {
        synchronized (lock) {
            if (!timerStatus) {
                segmentNumber++;
                timerStatus = true;
                lastTime = SystemClock.uptimeMillis();
            }
        }
    }

    public void timerPause() {
        synchronized (lock) {
            if (timerStatus) {
                timerStatus = false;
            }
        }
    }

    @Override
    public void run() {
        synchronized (lock) {
            long actualTime = SystemClock.uptimeMillis();
            if (timerStatus) {
                totalTime += actualTime - lastTime;

                double timeInSeconds = totalTime / 1000d;
                if (timeInSeconds < 1)
                    avgSpeed = 0;
                else
                    avgSpeed = totalDistance / timeInSeconds;
            }

            lastTime = actualTime;

            Intent data = new Intent(GPS_DATA);
            data.putExtra("speed", speed);
            data.putExtra("maxSpeed", maxSpeed);
            data.putExtra("avgSpeed", avgSpeed);
            data.putExtra("totalDistance", totalDistance);
            data.putExtra("altitude", altitude);
            data.putExtra("totalTime", totalTime);
            sendBroadcast(data);

            System.out.println("---------------------");
            System.out.println(speed);
            System.out.println(maxSpeed);
            System.out.println(avgSpeed);
            System.out.println(totalDistance);
            System.out.println(altitude);
            System.out.println(totalTime);
            System.out.println("---------------------");

            handler.postDelayed(this, 1000);
        }
    }

    public Long getActualTrackId() {
        synchronized (lock) {
            return track.getId();
        }
    }
}
