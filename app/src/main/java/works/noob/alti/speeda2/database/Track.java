package works.noob.alti.speeda2.database;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.OrderBy;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.Date;
import java.util.List;

/**
 * Created by kamil on 24.12.16.
 */

@Entity
public class Track {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    @NotNull
    private Double avgSpeed;

    @NotNull
    private Double maxSpeed;

    @NotNull
    private Double distance;

    @NotNull
    private Long trackTime;

    @ToMany(referencedJoinProperty = "trackId")
    @OrderBy("id ASC")
    private List<Point> pointList;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 506689419)
    private transient TrackDao myDao;

    @Generated(hash = 810718061)
    public Track(Long id, @NotNull Date startDate, @NotNull Date endDate,
            @NotNull Double avgSpeed, @NotNull Double maxSpeed, @NotNull Double distance,
            @NotNull Long trackTime) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.avgSpeed = avgSpeed;
        this.maxSpeed = maxSpeed;
        this.distance = distance;
        this.trackTime = trackTime;
    }

    @Generated(hash = 1672506944)
    public Track() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAvgSpeed() {
        return this.avgSpeed;
    }

    public void setAvgSpeed(Double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public Double getMaxSpeed() {
        return this.maxSpeed;
    }

    public void setMaxSpeed(Double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Double getDistance() {
        return this.distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getTrackTime() {
        return this.trackTime;
    }

    public void setTrackTime(Long trackTime) {
        this.trackTime = trackTime;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1946188504)
    public List<Point> getPointList() {
        if (pointList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PointDao targetDao = daoSession.getPointDao();
            List<Point> pointListNew = targetDao._queryTrack_PointList(id);
            synchronized (this) {
                if (pointList == null) {
                    pointList = pointListNew;
                }
            }
        }
        return pointList;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 18518400)
    public synchronized void resetPointList() {
        pointList = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1269964033)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getTrackDao() : null;
    }

}
