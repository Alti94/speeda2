package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 25.12.16.
 */

public class KiloMeterUnit extends Unit {
    private static final String DISTANCE_UNIT = "km";
    private static final String SPEED_UNIT = "km/h";
    private static final String ALTITUDE_UNIT = "m";

    @Override
    public String getDistanceUnit() {
        return DISTANCE_UNIT;
    }

    @Override
    public String getSpeedUnit() {
        return SPEED_UNIT;
    }

    @Override
    public String getAltitudeUnit() {
        return ALTITUDE_UNIT;
    }

    @Override
    public String getDistanceValue(double distanceInMeter) {
        double kilometer = distanceInMeter / 1000d;
        return String.format("%.1f", kilometer);
    }

    @Override
    public String getSpeedValue(double speedInMeterPerSecond) {
        double kilometerPerHour = speedInMeterPerSecond * 3.6d;
        return String.format("%.1f", kilometerPerHour);
    }

    @Override
    public String getAltitudeValue(double altitudeInMeter) {
        return String.format("%.1f", altitudeInMeter);
    }
}
