package works.noob.alti.speeda2;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import works.noob.alti.speeda2.database.Track;
import works.noob.alti.speeda2.units.TimeConverter;
import works.noob.alti.speeda2.units.Unit;
import works.noob.alti.speeda2.units.UnitFactory;

/**
 * Created by kamil on 30.12.16.
 */

public class TrackStatisticsActivity extends AppCompatActivity {
    public static final String TRACK_ID = "track_id";
    public static final String TRACK_SEGMENTS = "track_segments";
    public static final String AXIS_X = "axis_x";
    public static final String AXIS_Y = "axis_y";
    public static final String SELECTED_SEGMENT = "selected_segment";
    public static final int DISTANCE_AXIS = R.id.distance_axis;
    public static final int SPEED_AXIS = R.id.speed_axis;
    public static final int TIME_AXIS = R.id.time_axis;
    public static final int ALTITUDE_AXIS = R.id.altitude_axis;


    private static final String TAG = "STATISTICS";
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static final int WHOLE_TRACK = 0;

    private Track track;

    private long trackId;
    private int selectedSegment;
    private int segmentCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_statistics);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Unit units = UnitFactory.createUnit(preferences.getString(SettingsActivity.UNITS, null));

        Intent activityIntent = getIntent();
        trackId = activityIntent.getLongExtra(TRACK_ID, -1);
        segmentCount = activityIntent.getIntExtra(TRACK_SEGMENTS, 0);
        track = ((App) getApplication()).getDaoSession().getTrackDao().load(trackId);
        if (track != null) {
            ((TextView) findViewById(R.id.start_date)).setText(dateFormat.format(track.getStartDate()));
            ((TextView) findViewById(R.id.end_date)).setText(dateFormat.format(track.getEndDate()));
            ((TextView) findViewById(R.id.track_time)).setText(TimeConverter.convert(track.getTrackTime()));
            ((TextView) findViewById(R.id.distance)).setText(units.convertDistance(track.getDistance()));
            ((TextView) findViewById(R.id.avg_speed)).setText(units.convertSpeed(track.getAvgSpeed()));
            ((TextView) findViewById(R.id.max_speed)).setText(units.convertSpeed(track.getMaxSpeed()));

            ((TextView) findViewById(R.id.textView2)).setText(getString(R.string.whole_track));
            selectedSegment = WHOLE_TRACK;
        }
        else {
            Log.e(TAG, "Wrong id");
            finish();
        }
    }

    public void selectSegment(View view) {
        int elementCount = segmentCount + 1;
        CharSequence[] elements = new CharSequence[elementCount];
        for (int i = 0; i < elementCount; i++) {
            if (i == 0)
                elements[i] = getString(R.string.whole_track);
            else
                elements[i] = Integer.toString(i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.select_segment));
        builder.setItems(
                elements,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedSegment = which;
                        updateSelectedSegment();
                    }
                }
        );

        builder.show();
    }

    public void showChart(View view) {
        RadioGroup axisX = (RadioGroup) findViewById(R.id.x_axis);
        RadioGroup axisY = (RadioGroup) findViewById(R.id.y_axis);

        Intent chartIntent = new Intent(this, ChartActivity.class);
        chartIntent.putExtra(TRACK_ID, trackId);
        chartIntent.putExtra(AXIS_X, axisX.getCheckedRadioButtonId());
        chartIntent.putExtra(AXIS_Y, axisY.getCheckedRadioButtonId());
        chartIntent.putExtra(SELECTED_SEGMENT, selectedSegment);
        startActivity(chartIntent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final CharSequence startDate = ((TextView) findViewById(R.id.start_date)).getText();
        final CharSequence endDate = ((TextView) findViewById(R.id.end_date)).getText();
        final CharSequence trackTime = ((TextView) findViewById(R.id.track_time)).getText();
        final CharSequence distance = ((TextView) findViewById(R.id.distance)).getText();
        final CharSequence avgSpeed = ((TextView) findViewById(R.id.avg_speed)).getText();
        final CharSequence maxSpeed = ((TextView) findViewById(R.id.max_speed)).getText();
        final CharSequence selectedSegment = ((TextView) findViewById(R.id.textView2)).getText();

        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_track_statistics);

        ((TextView) findViewById(R.id.start_date)).setText(startDate);
        ((TextView) findViewById(R.id.end_date)).setText(endDate);
        ((TextView) findViewById(R.id.track_time)).setText(trackTime);
        ((TextView) findViewById(R.id.distance)).setText(distance);
        ((TextView) findViewById(R.id.avg_speed)).setText(avgSpeed);
        ((TextView) findViewById(R.id.max_speed)).setText(maxSpeed);
        ((TextView) findViewById(R.id.textView2)).setText(selectedSegment);
    }

    private void updateSelectedSegment() {
        if (selectedSegment == 0) {
            ((TextView) findViewById(R.id.textView2)).setText(getString(R.string.whole_track));
        }
        else {
            ((TextView) findViewById(R.id.textView2)).setText(selectedSegment + "/" + segmentCount);
        }
    }
}
