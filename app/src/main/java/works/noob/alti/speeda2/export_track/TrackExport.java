package works.noob.alti.speeda2.export_track;

import works.noob.alti.speeda2.database.Point;

/**
 * Created by kamil on 29.12.16.
 */

public interface TrackExport {
    public void addPoint(Point point);
    public String endTrack();
}
