package works.noob.alti.speeda2.units;

/**
 * Created by kamil on 25.12.16.
 */

public class MilesUnit extends Unit {
    private static final String DISTANCE_UNIT = "mi";
    private static final String SPEED_UNIT = "MPH";
    private static final String ALTITUDE_UNIT = "ft";

    @Override
    public String getDistanceUnit() {
        return DISTANCE_UNIT;
    }

    @Override
    public String getSpeedUnit() {
        return SPEED_UNIT;
    }

    @Override
    public String getAltitudeUnit() {
        return ALTITUDE_UNIT;
    }

    @Override
    public String getDistanceValue(double distanceInMeter) {
        double mile = distanceInMeter * 0.000621371192d;
        return String.format("%.1f", mile);
    }

    @Override
    public String getSpeedValue(double speedInMeterPerSecond) {
        double mph = speedInMeterPerSecond * 2.23693629d;
        return String.format("%.1f", mph);
    }

    @Override
    public String getAltitudeValue(double altitudeInMeter) {
        double foot = altitudeInMeter * 3.2808399d;
        return String.format("%.1f", foot);
    }
}
