package works.noob.alti.speeda2;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import org.mapsforge.core.graphics.Filter;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.Dimension;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.MapPosition;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.layer.Layer;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.model.DisplayModel;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.reader.header.MapFileException;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import works.noob.alti.speeda2.database.DaoSession;
import works.noob.alti.speeda2.database.Point;
import works.noob.alti.speeda2.database.PointDao;
import works.noob.alti.speeda2.database.Track;
import works.noob.alti.speeda2.database.TrackDao;
import works.noob.alti.speeda2.export_track.ExportGpx;
import works.noob.alti.speeda2.export_track.TrackExport;
import works.noob.alti.speeda2.import_track.ImportGpx;
import works.noob.alti.speeda2.import_track.TrackImport;

/**
 * Created by kamil on 27.12.16.
 */

public class MapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String MAP_FILE = "Poland.map";
    public static final int PICK_TRACK_REQUEST = 1;
    public static final int PICK_IMPORT_FILE = 2;

    private MapView mapView;

    private TrackDao trackDao;
    private PointDao pointDao;

    private Track selectedTrack;
    private int segments;
    private List<Layer> actualShownTrack;

    private boolean isNightMode;
    boolean isRotationLock;
    boolean isScreenLock;

    private SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        DaoSession session = ((App) getApplication()).getDaoSession();
        trackDao = session.getTrackDao();
        pointDao = session.getPointDao();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mapView = (MapView) findViewById(R.id.map_view);

        isNightMode = false;
        isRotationLock = false;
        isScreenLock = false;

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void loadMap() {
        TileCache tileCache = AndroidUtil.createTileCache(
                this,
                "mapcache",
                mapView.getModel().displayModel.getTileSize(),
                1f,
                mapView.getModel().frameBufferModel.getOverdrawFactor()
        );

        String mapFileName = preferences.getString("mapFile", "dont_mind_it");
        File appDir = new File(Environment.getExternalStorageDirectory(), "Speeda2");
        File mapDir = new File(appDir, "maps");
        File mapFile = new File(mapDir, mapFileName);

        try {
            MapDataStore mapDataStore = new MapFile(mapFile);

            TileRendererLayer tileRendererLayer = new TileRendererLayer(
                    tileCache,
                    mapDataStore,
                    mapView.getModel().mapViewPosition,
                    AndroidGraphicFactory.INSTANCE
            );

            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.DEFAULT);
            mapView.getLayerManager().getLayers().add(tileRendererLayer);
        }
        catch (MapFileException e) {
            Toast.makeText(this, getString(R.string.map_file_error), Toast.LENGTH_LONG).show();
        }
    }

    private void initMap() {
        loadMap();
        this.mapView.setBuiltInZoomControls(true);
        this.mapView.setClickable(true);
        this.mapView.setZoomLevelMin((byte) 1);

        Location lastLocation = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            lastLocation = ((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        LatLong position;

        if (lastLocation == null)
            position = new LatLong(53.12d,23.15d);
        else
            position = new LatLong(lastLocation.getLatitude(), lastLocation.getLongitude());

        mapView.setCenter(position);
        mapView.setZoomLevel((byte) 10);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length == 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                } else {
                    initMap();
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        this.mapView.destroyAll();
        AndroidGraphicFactory.clearResourceMemoryCache();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_load_track:
                selectTrack();
                break;
            case R.id.nav_statistics:
                showStatistics();
                break;
            case R.id.nav_import:
                startActivityForResult(new Intent(this, FileSelectActivity.class), PICK_IMPORT_FILE);
                break;
            case R.id.nav_export:
                exportTrack();
                break;
            case R.id.nav_night_mode:
                nightMode(item);
                break;
            case R.id.nav_display_lock:
                displayLock(item);
                break;
            case R.id.nav_rotation_lock:
                lockRotation(item);
                break;
            case R.id.nav_app_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.nav_app_back:
                finish();
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_TRACK_REQUEST) {
            if (resultCode == RESULT_OK) {
                long id = data.getLongExtra(TrackSelectActivity.TRACK_ID, -1);
                selectedTrack = trackDao.load(id);
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle(getString(R.string.wait));
                progressDialog.setCancelable(false);
                progressDialog.setMessage(getString(R.string.track_loading));
                progressDialog.show();

                if (selectedTrack != null) {

                    showTrackOnMap(selectedTrack);
                }

                progressDialog.dismiss();
            }
        }
        else if (requestCode == PICK_IMPORT_FILE) {
            if (resultCode == RESULT_OK) {
                String filename = data.getStringExtra(FileSelectActivity.FILENAME);
                if (filename != null) {
                    File file = new File(new File(Environment.getExternalStorageDirectory(), "Speeda2"), filename);
                    importTrack(file);
                }
            }
        }
    }

    private void selectTrack() {
        startActivityForResult(new Intent(this, TrackSelectActivity.class), PICK_TRACK_REQUEST);
    }

    private void showTrackOnMap(@NonNull Track track) {
        Paint paint = AndroidGraphicFactory.INSTANCE.createPaint();
        paint.setColor(preferences.getInt("trackColor", 0xFFFF0000));
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth(8);

        Layers mapLayers = mapView.getLayerManager().getLayers();
        if (actualShownTrack != null) {
            for (Layer layer : actualShownTrack)
                mapLayers.remove(layer);
            actualShownTrack.clear();
        }
        else {
            actualShownTrack = new ArrayList<>();
        }

        Polyline trackLine = new Polyline(paint, AndroidGraphicFactory.INSTANCE);
        List<LatLong> latLongs = trackLine.getLatLongs();

        List<Point> trackPoints = selectedTrack.getPointList();

        segments = 0;
        if (!trackPoints.isEmpty()) {
            segments = trackPoints.get(0).getSegmentNumber();

            double maxLat = Double.MIN_VALUE;
            double maxLong = Double.MIN_VALUE;
            double minLat = Double.MAX_VALUE;
            double minLong = Double.MAX_VALUE;

            for (Point point : trackPoints) {
                if (segments != point.getSegmentNumber()) {
                    actualShownTrack.add(trackLine);
                    mapLayers.add(trackLine);

                    trackLine = new Polyline(paint, AndroidGraphicFactory.INSTANCE);
                    latLongs = trackLine.getLatLongs();
                    segments = point.getSegmentNumber();
                }
                latLongs.add(point.getLatLong());

                if (maxLat < point.getLatitude())
                    maxLat = point.getLatitude();
                if (maxLong < point.getLongitude())
                    maxLong = point.getLongitude();
                if (minLat > point.getLatitude())
                    minLat = point.getLatitude();
                if (minLong > point.getLongitude())
                    minLong = point.getLongitude();
            }

            actualShownTrack.add(trackLine);
            mapLayers.add(trackLine);

            BoundingBox boundingBox = new BoundingBox(minLat, minLong, maxLat, maxLong);
            Dimension dimension = mapView.getModel().mapViewDimension.getDimension();

            mapView.getModel().mapViewPosition.setMapPosition(
                    new MapPosition(
                            boundingBox.getCenterPoint(),
                            LatLongUtils.zoomForBounds(dimension,
                                    boundingBox,
                                    mapView.getModel().displayModel.getTileSize()
                            )
                    )
            );
        }
    }

    private void importTrack(File file) {
        //Track

        final File filef = file;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.wait));
        progressDialog.setMessage(getString(R.string.import_in_progress));
        progressDialog.setCancelable(false);
        progressDialog.show();

        new Thread() {
            @Override
            public void run() {
                TrackImport trackImport = new ImportGpx();
                final Track track = trackImport.importTrack(filef);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();

                        if (track != null) {
                            Toast.makeText(getApplicationContext(), getString(R.string.import_success) + ": " + track.getStartDate().toString(), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), getString(R.string.import_failiure), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }.start();
    }

    private void exportTrack() {
        final File exportDir = new File(Environment.getExternalStorageDirectory(), "Speeda2");
        if (!exportDir.exists())
            exportDir.mkdirs();

        if (selectedTrack != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.wait));
            progressDialog.setMessage(getString(R.string.export_in_progress));
            progressDialog.setCancelable(false);
            progressDialog.show();

            new Thread() {
                @Override
                public void run() {
                    TrackExport trackExport = new ExportGpx(selectedTrack, exportDir);
                    for (Point point : selectedTrack.getPointList())
                        trackExport.addPoint(point);
                    final String fileName = trackExport.endTrack();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.export_success) + ": " + fileName, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }.start();
        }
        else {
            Toast.makeText(this, getString(R.string.not_selected_track), Toast.LENGTH_SHORT).show();
        }
    }

    private void nightMode(MenuItem item) {
        DisplayModel displayModel = mapView.getModel().displayModel;
        if (isNightMode = !isNightMode) {
            displayModel.setFilter(Filter.INVERT);
            displayModel.setBackgroundColor(0xFF000000);
            item.setTitle(getString(R.string.day_mode));
            item.setIcon(R.drawable.ic_menu_day_mode);
        }
        else {
            displayModel.setFilter(Filter.NONE);
            displayModel.setBackgroundColor(0xFFFFFFFF);
            item.setTitle(getString(R.string.night_mode));
            item.setIcon(R.drawable.ic_menu_night_mode);
        }
    }

    private void displayLock(MenuItem item) {
        if (isScreenLock = !isScreenLock) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            item.setTitle(getString(R.string.unlock_screen));
            item.setIcon(R.drawable.ic_menu_screen_unlock);
        }
        else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            item.setTitle(getString(R.string.lock_screen));
            item.setIcon(R.drawable.ic_menu_screen_lock);
        }
    }

    private void lockRotation(MenuItem item) {
        if (!isRotationLock) {
            setRequestedOrientation(SpeedometerActivity.getInstance().getScreenOrientation());
            item.setIcon(R.drawable.ic_menu_unlock_rotation);
            item.setTitle(getString(R.string.unlock_rotation));
            isRotationLock = true;
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            item.setIcon(R.drawable.ic_menu_lock_rotation);
            item.setTitle(R.string.lock_rotation);
            isRotationLock = false;
        }
    }

    private void showStatistics() {
        if (selectedTrack != null) {
            Intent statisticsIntent = new Intent(this, TrackStatisticsActivity.class);
            statisticsIntent.putExtra(TrackStatisticsActivity.TRACK_ID, selectedTrack.getId());
            statisticsIntent.putExtra(TrackStatisticsActivity.TRACK_SEGMENTS, segments);
            startActivity(statisticsIntent);
        }
        else {
            Toast.makeText(this, getString(R.string.not_selected_track), Toast.LENGTH_SHORT).show();
        }
    }
}
